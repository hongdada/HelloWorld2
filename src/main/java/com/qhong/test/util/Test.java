package com.qhong.test.util;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author qhong
 * @date 2022/8/25 17:26
 **/
public class Test {

	public static void main(String[] args) {
		System.out.println(TestA.builder().build());

		System.out.println(new TestA());


	}
}

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
class TestA {

	@Builder.Default
	private String a = "aa";
	private Integer age = 22;

	private String b;
}
