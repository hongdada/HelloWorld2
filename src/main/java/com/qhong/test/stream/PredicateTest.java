package com.qhong.test.stream;

import java.util.function.Predicate;

/**
 * Created by qhong on 2018/11/26 17:25
 **/
public class PredicateTest {
	public void printBigValue(int value, Predicate<Integer> predicate) {
		if (predicate.test(value)) {
			System.out.println(value);
		}
	}
	public void printBigValueAnd(int value, Predicate<Integer> predicate) {
		if (predicate.and(v -> v < 8).test(value)) {
			System.out.println("value < 8 : " + value);
		} else {
			System.out.println("value should < 8 at least.");
		}
	}
}
