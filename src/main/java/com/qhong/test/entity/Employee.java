package com.qhong.test.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by qhong on 2018/8/3 18:17
 **/
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Employee {

	private Integer id;
	private Integer age;
	private String gender;
	private String firstName;
	private String lastName;
}
