package com.qhong.test.base;

import java.util.List;
import lombok.Data;

/**
 * Created by qhong on 2018/8/3 18:24
 **/
@Data
public final class BootPage<T> {

	private Long total;

	private List<T> rows;

//	public BootPage() {
//	}
//
//	public Long getTotal() {
//		return this.total;
//	}
//
//	public void setTotal(Long total) {
//		this.total = total;
//	}
//
//	public List<T> getRows() {
//		return this.rows;
//	}
//
//	public void setRows(List<T> rows) {
//		this.rows = rows;
//	}
}
