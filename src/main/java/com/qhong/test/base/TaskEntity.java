package com.qhong.test.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * Created by qhong on 2018/8/6 10:02
 **/
public class TaskEntity implements Serializable {
	private static final long serialVersionUID = 4350404547654317453L;

	@JsonProperty(
			value = "taskId",
			required = true
	)
	private String taskId;

	@JsonProperty(
			value = "processInstanceId",
			required = true
	)
	private String processInstanceId;

	@JsonProperty(
			value = "taskName",
			required = true
	)
	private String taskName;

	@JsonProperty(
			value = "userName",
			required = true
	)
	private String userName;

	@JsonProperty(
			value = "taskCreateTime",
			required = true
	)
	private Date taskCreateTime;

	@JsonProperty(
			value = "taskEndTime",
			required = true
	)
	private Date taskEndTime;

	@JsonProperty(
			value = "otherAttributes",
			required = true
	)
	private Map<String, Object> otherAttributes;

	public TaskEntity() {
	}

	public String getProcessInstanceId() {
		return this.processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return this.taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return this.taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Map<String, Object> getOtherAttributes() {
		return this.otherAttributes;
	}

	public void setOtherAttributes(Map<String, Object> otherAttributes) {
		this.otherAttributes = otherAttributes;
	}

	public Date getTaskCreateTime() {
		return this.taskCreateTime;
	}

	public void setTaskCreateTime(Date taskCreateTime) {
		this.taskCreateTime = taskCreateTime;
	}

	public Date getTaskEndTime() {
		return this.taskEndTime;
	}

	public void setTaskEndTime(Date taskEndTime) {
		this.taskEndTime = taskEndTime;
	}
}
