package com.qhong.test.stream;

import com.alibaba.fastjson.JSON;
import com.qhong.test.data.EmployeeListData;
import com.qhong.test.entity.Employee;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

/**
 * Created by qhong on 2018/9/6 16:20
 **/
@Slf4j
public class test1 {

	@Test
	public void test(){
		Integer totalAges=EmployeeListData.getEmployeeList().stream().collect(Collectors.summingInt(x->x.getAge()));
		System.out.println("age:"+totalAges);
		//Integer tt= EmployeeListData.getEmployeeList().stream().collect(Collectors.summarizingInt(Employee::getAge));
		Integer totalAges2=EmployeeListData.getEmployeeList().stream().mapToInt(x->x.getAge()).sum();
		System.out.println("age2:"+totalAges2);

		EmployeeListData.getEmployeeList().stream().filter(x->x.getAge()>20);

		System.out.println(EmployeeListData.getEmployeeList().size());

	}

	@Test
	public void testReplace(){
		List<String> list=EmployeeListData.getEmployeeList().stream().map(x->x.getFirstName().replace("R","")).collect(Collectors.toList());
		String json= JSON.toJSONString(list);
		log.info(json);
	}

	/**
	 * 测试从lambda表达式引用的本地变量必须是最终变量
	 */
	@Test
	public void testFinal(){
		String aa="aa";
		aa="bb";
//		List<String> list= Lists.newArrayList();
//		list=new ArrayList<>();
		EmployeeListData.getEmployeeList().forEach(x->{
			log.info(x.getFirstName());
            //aa=x.getFirstName();
			//list.add(x.getFirstName());
		});
		log.info(aa);
	}

	@Test
	public void testListObject(){
		Employee ee = new Employee(5, 19, "F", "aaa", "Maria");
		List<Employee> list= EmployeeListData.getEmployeeList();
		list.forEach(x->{
			if(x.getId()==ee.getId()){
				x.setFirstName(ee.getFirstName());
			}
		});
		log.info(JSON.toJSONString(list));
	}



	@Test
	public void testStreamEffect(){
		List<Integer> list= Arrays.asList(1,2,3,4,5);
		list.stream().forEach(x->x+=1);
		log.info(Arrays.toString(list.toArray()));
	}

	@Test
	public void testFunctionalInterface(){
//		Function<Integer, Integer> incr1 = x -> x + 1;
//		Function<Integer, Integer> multiply = x -> x * 2;
//		int x = 2;
//		System.out.println("f(x)=x+1,when x=" + x + ", f(x)=" + incr1.apply(x));
//		System.out.println("f(x)=x+1,g(x)=2x, when x=" + x + ", f(g(x))=" + incr1.compose(multiply).apply(x));
//		System.out.println("f(x)=x+1,g(x)=2x, when x=" + x + ", g(f(x))=" + incr1.andThen(multiply).apply(x));
//		System.out.println("compose vs andThen:f(g(x))=" + incr1.compose(multiply).apply(x) + "," + multiply.andThen(incr1).apply(x));
//
//		//binary function
//		BiFunction<Integer, Integer, Integer> biMultiply = (a, b) -> a * b;
//		System.out.println("f(z)=x*y, when x=3,y=5, then f(z)=" + biMultiply.apply(3, 5));

//		UnaryOperator<Integer> add = x -> x + 1;
//		System.out.println(add.apply(1));
//		BinaryOperator<Integer> addxy = (x, y) -> x + y;
//		System.out.println(addxy.apply(3, 5));
//		BinaryOperator<Integer> min = BinaryOperator.minBy((o1, o2) -> o1 - o2);
//		System.out.println(min.apply(100, 200));
//		BinaryOperator<Integer> max = BinaryOperator.maxBy((o1, o2) -> o1 - o2);
//		System.out.println(max.apply(100, 200));

//		PredicateTest testJ8Predicate = new PredicateTest();
//		testJ8Predicate.printBigValue(10, val -> val > 5);
//		testJ8Predicate.printBigValueAnd(10, val -> val > 5);
//		testJ8Predicate.printBigValueAnd(6, val -> val > 5);
//		//binary predicate
//		BiPredicate<Integer, Long> biPredicate = (x, y) -> x > 9 && y < 100;
//		System.out.println(biPredicate.test(100, 50L));

//		Consumer<Integer> consumer = System.out::println;
//		consumer.accept(100);
//		//use function, you always need one return value.
//		Function<Integer, Integer> function = x -> {
//			System.out.println(x);
//			return x;
//		};
//		function.apply(100);

		Supplier<Integer> supplier = () -> 1;
		System.out.println(supplier.get());
	}

	@Test
	public  void testSplit(){
		String ids="1,2,3,4,5,6";
		List<Integer> issueIds=Arrays.asList(ids.split(",")).stream().map(x->Integer.parseInt(x)).collect(Collectors.toList());
		log.info(JSON.toJSONString(issueIds));
	}

	@Test
	public void testJoin(){
		String content=Arrays.stream(new String[]{"a","",null,"asdf","b"}).filter(y-> StringUtils.isNotBlank(y)).distinct().collect(Collectors.joining(","));
		log.info(content);
		String content2=Stream.of("a","b").collect(Collectors.joining(","));
		log.info(content2);
	}

}

