package com.qhong.test;

import javax.annotation.Resource;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Created by qhong on 2018/6/12 16:49
 * @SpringBootTest 会配置spring boot环境而@ContextConfiguration 只会配置spring环境
 * http://guoweisharp.iteye.com/blog/2388134
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
//@SpringBootTest(classes=SpringBootDemoApplication.class)// 指定spring-boot的启动类
//@SpringApplicationConfiguration(classes = SpringBootDemoApplication.class)// 1.4.0 前版本

public abstract class BaseTest {
	@Resource
	private WebApplicationContext context;
	public MockMvc mvc;

	@Before
	public void setupMockMvc() {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
	}
}