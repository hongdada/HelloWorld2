package com.qhong.test.config;

import com.qhong.test.data.EmployeeListData;
import com.qhong.test.entity.Employee;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration.AccessLevel;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.convention.NamingConventions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by qhong on 2018/8/7 10:05
 **/
@Slf4j
@Configuration
public class BeanConfig {
	@Bean
	public ModelMapper getModelMapper(){
		ModelMapper modelMapper=new ModelMapper();
		modelMapper.getConfiguration()
				.setFieldMatchingEnabled(false)
				//匹配私有字段
				.setFieldAccessLevel(AccessLevel.PRIVATE)
				//松散的匹配源和目标属性
				//.setMatchingStrategy(MatchingStrategies.LOOSE)
				//严格的匹配源和目标属性
				.setMatchingStrategy(MatchingStrategies.STRICT)
				//代表没有命名约定,这适用于所有属性名
				.setSourceNamingConvention(NamingConventions.NONE)
				.setDestinationNamingConvention(NamingConventions.NONE);
		return modelMapper;
	}

	@Bean(name="getEmployeeList")
	public List<Employee> getEmployeeList(){

		log.info("init success");
		return EmployeeListData.getEmployeeList();
	}
}
