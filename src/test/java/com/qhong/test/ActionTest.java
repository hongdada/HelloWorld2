package com.qhong.test;


import com.alibaba.fastjson.JSON;
import com.qhong.test.data.EmployeeListData;
import com.qhong.test.entity.Employee;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Created by qhong on 2018/6/12 16:51
 **/
public class ActionTest  {

	@Autowired
	@Qualifier("getEmployeeList")
	private List<Employee> employeeList;

    @Autowired
	private ModelMapper modelMapper;

    @Test
    public void test(){
//        String message =String.format("{'%s':'%s'}","aaa",5);
//		System.out.println(message);

//	  String message2=MessageFormat.format("用户名：{},提交流程审批,节点名称：{}，订单号：{}，出现异常，错误信息：{}","hongdada","nodeName","loanOrderId","error");
//		System.out.println(message2);
//
		String message3=String.format("用户名：%s,提交流程审批,节点名称：%s，订单号：%s，出现异常，错误信息：%s","hongdada","nodeName","loanOrderId","error");
		System.out.println(message3);
	}


	@Test
	public void test2(){
		System.out.println(JSON.toJSONString(employeeList));
	}

	/**
	 * forEach 无法中断
	 */
	@Test
	public void testForEach(){
		EmployeeListData.getEmployeeList().forEach(x->{
			System.out.println(x.getId());
			if(x.getId()==4){
				return;
			}
		});
		System.out.println("end");
		EmployeeListData.getEmployeeList().stream().forEach(x->{
			System.out.println(x.getId());
			if(x.getId()==4){
				return;
			}
		});
	}

	@Test
	public void testForEach2(){
		for(Employee x:EmployeeListData.getEmployeeList()){
			System.out.println(x.getId());
			if(x.getId()==4){
				if(true){
					break;
				}

			}
		}
	}


	@Test
    public void testSort(){
		List<Employee> list=EmployeeListData.getEmployeeList()
				.stream()
				.sorted(Comparator.comparing(x->x.getAge()))
				.collect(Collectors.toList());
		System.out.println(JSON.toJSONString(list));
	}

	@Test
	public void testSortDesc(){
		List<Employee> list=EmployeeListData.getEmployeeList()
				.stream()
				.sorted(Comparator.comparing(Employee::getAge).reversed())
				.collect(Collectors.toList());
		System.out.println(JSON.toJSONString(list));
	}

	@Test
	public void testBeanUtils(){
		Employee employee=EmployeeListData.getEmployeeList().get(0);
		Employee employee1=new Employee();
		BeanUtils.copyProperties(employee,employee1);
		System.out.println(JSON.toJSONString(employee1));
		employee.setAge(222);
		System.out.println(employee.equals(employee1));
	}

	@Test
	public void testModelMapper(){
        Employee employee=EmployeeListData.getEmployeeList().get(0);
        Employee employee1=modelMapper.map(employee,Employee.class);
		System.out.println(employee.equals(employee1));
	}

	@Test
	public void testCollect(){
		List<String> employeeNames = EmployeeListData.getEmployeeList().stream()
		.collect(Collectors.mapping(Employee::getFirstName, Collectors.toList()));
		System.out.println("List of employee names:" + employeeNames);

	}

}