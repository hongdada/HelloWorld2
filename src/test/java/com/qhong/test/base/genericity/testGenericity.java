package com.qhong.test.base.genericity;

import com.alibaba.fastjson.JSON;
import com.qhong.test.base.BootPage;
import com.qhong.test.base.TaskEntity;
import com.qhong.test.data.EmployeeListData;
import com.qhong.test.entity.Employee;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * Created by qhong on 2018/8/3 18:25
 **/
@Slf4j
public class testGenericity {

	@Test
	public void test(){
		Employee e1 = new Employee(1, 23, "M", "Rick", "Beethovan");
		Employee e2 = new Employee(2, 13, "F", "Martina", "Hengis");
		Employee e3 = new Employee(3, 43, "M", "Ricky", "Martin");
		Employee e4 = new Employee(4, 26, "M", "Jon", "Lowman");
		Employee e5 = new Employee(5, 19, "F", "Cristine", "Maria");
		Employee e6 = new Employee(6, 15, "M", "David", "Feezor");
		Employee e7 = new Employee(7, 68, "F", "Melissa", "Roy");
		Employee e8 = new Employee(8, 79, "M", "Alex", "Gussin");
		Employee e9 = new Employee(9, 15, "F", "Neetu", "Singh");
		Employee e10 = new Employee(10, 45, "M", "Naveen", "Jain");

		List<Employee> employees = new ArrayList<Employee>();
		employees.addAll(Arrays.asList(new Employee[]{e1, e2, e3, e4, e5, e6, e7, e8, e9, e10}));
		BootPage<Employee> bootPage=new BootPage<>();
		bootPage.setRows(employees);
		Employee ea=bootPage.getRows().stream().filter(x->x.getId()==1).findFirst().orElse(null);
		String name=bootPage.getRows().stream().filter(x->x.getId()==1).map(x->x.getFirstName()).findFirst().orElse(null);
		System.out.println(name);
		List<String> names=bootPage.getRows().stream().map(x->x.getFirstName()).collect(Collectors.toList());
		System.out.println(JSON.toJSONString(names));




	}

	@Test
	public void TestTaskEntity(){
		String content="{\n"
				+ " \t\t\t\"taskId\": \"287118\",\n"
				+ " \t\t\t\"processInstanceId\": \"287109\",\n"
				+ " \t\t\t\"taskName\": \"业务部派单\",\n"
				+ " \t\t\t\"taskCreateTime\": 1533278841248,\n"
				+ " \t\t\t\"otherAttributes\": {\n"
				+ " \t\t\t\t\"loanOrderId\": \"BO118628882254528526\"\n"
				+ " \t\t\t}\n"
				+ " \t\t}";
		TaskEntity taskEntity=  JSON.parseObject(content,TaskEntity.class);

		String content2="{\n"
				+ " \t\t\t\"taskId\": \"287092\",\n"
				+ " \t\t\t\"processInstanceId\": \"287083\",\n"
				+ " \t\t\t\"taskName\": \"业务部派单\",\n"
				+ " \t\t\t\"taskCreateTime\": 1533276348152,\n"
				+ " \t\t\t\"otherAttributes\": {\n"
			//	+ " \t\t\t\t\"loanOrderId\": \"PO118538122243997700\"\n"
				+ " \t\t\t}\n"
				+ " \t\t}";
		TaskEntity taskEntity1= JSON.parseObject(content2,TaskEntity.class);
		List<TaskEntity> list=Arrays.asList(new TaskEntity[]{taskEntity,taskEntity1});
		BootPage<TaskEntity> bootPage=new BootPage<>();
		bootPage.setRows(list);
	    List<String> ids=bootPage.getRows().stream().map(x->x.getProcessInstanceId()).collect(Collectors.toList());
		System.out.println(ids);
//	    List<String> ids2=	bootPage.getRows().stream().flatMap(x->x.getTaskId()).collect(Collectors.toList());
//		System.out.println(ids2);
		//taskEntity.setOtherAttributes(null);

	}

	@Test
	public void testString(){
		String str="http://shitou-huishi.oss-cn-hangzhou.aliyuncs.com/huishi_test/%E4%BC%81%E4%B8%9A%E4%BF%A1%E6%81%AF/%E8%AE%A2%E5%8D%95%E5%9B%BE%E7%89%87/1515581975262.png?Expires=1515583793&OSSAccessKeyId=STS.HBHgkSR6Jhus1p5TaJE4x4GWM&Signature=apqAJE5m7iuxhQng68PFc/T7qkY%3D&security-token=CAISjwJ1q6Ft5B2yfSjIqYH9LNHnv%2Blr37eYM1aEsGEfSbtU24L8rzz2IHlOe3NgCe8XtvQ/nmtX7fgZlrh%2BW4NIX0rNaY5t9ZlN9wqkbtJBJ2YiCPhW5qe%2BEE2/VjTdvqaLEcSbIfrZfvCyESem8gZ43br9cxi7QlWhKufnoJV7b9MRLGbaAD1dH4UUXHwAzvUXLnzML/2gHwf3i27LdipStxF7lHl05NbUoKTeyGKH1gOjkb5K/dyhfMD4PpE9BvolDYfpht4RX7HazStd5yJN8KpLl6Fe8V/FxIrEWQYLs0XZYrKEooc0c1QnPbJJEqpFveX6kuZjpuvQmoL4xhBAJ%2BhJVD7FQ4St0E5t2G9/uSBTGoABkGFDlehnO/GXi5AnPKnrgbuUXXWaQlY00dQ76VLeuXchpeW2861GHZVbu25Wu40QXcBpxgWvCDVfu1V6Vg9y9vZd/LGHvozi2VnGhk2qIYZWGM0U%2BAuaoAuEy4xaMNeav3T1l6ewXP94QzI9j5F/ZGE5BJxTTNMt35x7V8KJIoI%3D";
		String url=str.split("\\?")[0];
		System.out.println(url);
	}

	@Test
	public void testSubStirng(){
		String line="*asdfa**asdfsd**sad**";
		String line2=line.substring(1,line.length()-1);
		line2=line2.replaceAll("\\*","");
		System.out.println(line2);
		String result=line.substring(0,1)+line2+line.substring(line.length()-1,line.length());
		System.out.println(result);

	}

	@Test
	public void testMoney(){
//		String abc = "价55格：0.00元0.06asdfasdf";
//		Pattern compile = Pattern.compile("\\d+\\.\\d+");
//		Matcher matcher = compile.matcher(abc);
//		matcher.find();
//		String string = matcher.group();//提取匹配到的结果
//		System.out.println(string);//0.00


		String str = "asd2312.3fasf-0.11as4fa+234.3sdf";

		//String regxp = "[^(-)?(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){1,2})?]";
		String regxp = "((-)?(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){1,2})?)";


		Pattern p = Pattern.compile(regxp);
		Matcher m = p.matcher(str);
		while (m.find()) {
			String pStr = m.group();
			System.out.println(pStr);
		}


//		Pattern pattern=Pattern.compile("^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){0,2})?$"); // 判断小数点后2位的数字的正则表达式
//		Matcher match=pattern.matcher(str);


	}

	@Test
	public void testConsole(){
		log.info("aa{}bb{}","11",22);
		//log.info("aa{1}bb{0}","11",22);
		log.info(MessageFormat.format("aa{1}bb{0}",11,22));

	}

	@Test
	public void testFloat(){
		Float f=-5.5F;
		log.info(String.valueOf(f));
		Double d=-5.66666666321D;
		log.info(String.valueOf(d));
	}

	@Test
	public void testBitOperation(){
		int i = 234;

		byte b = (byte) i; // 结果：b = -22

		log.info(b+"");
	}

	@Test
	public void testRemove() {
		List<Employee> list = EmployeeListData.getEmployeeList();
		Iterator<Employee> it = list.iterator();
		while (it.hasNext()){
			Employee ee=it.next();
			if(ee.getId().equals(4)){
				it.remove();
				it.remove();
			}
		}
		log.info(JSON.toJSONString(list));
	}


}
